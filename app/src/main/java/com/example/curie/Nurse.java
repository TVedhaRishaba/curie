package com.example.curie;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.curie.model.Patient;
import com.example.curie.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Nurse extends AppCompatActivity {

    private static final String TAG = "NurseScreen";
    List<Patient> patData;
    ProgressDialog dialog;
    FloatingActionButton add_patient_fab;
    RecyclerView pat_recyclerView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigation;

    TextView hname,pat_count;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nurse);
        add_patient_fab = (FloatingActionButton) findViewById(R.id.add_patient_fab);




        drawerLayout =(DrawerLayout)findViewById(R.id.drawer);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (Exception e){
            Log.d(TAG, "onCreate: ",e.fillInStackTrace() );
        }

        add_patient_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Nurse.this, AddPatient.class));


            }
        });


        navigation = (NavigationView) findViewById(R.id.navigation);
        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id) {
                    case R.id.logout:
                        startActivity(new Intent(Nurse.this, WelcomeScreen.class));
                        finish();

                        break;
                }
                return false;
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();

        patData = new ArrayList<>();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading Patients...");
        dialog.show();

        hname=(TextView) findViewById(R.id.hname);
        pat_count=(TextView)findViewById(R.id.pat_count);
        User modal = PreferenceManager.getCurrentUser(this);
        try {
            hname.setText(modal.getHospital());
        }catch (Exception e){

        }
        pat_recyclerView = (RecyclerView) findViewById(R.id.org_list);
        pat_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        PatParsing();


    }

    public void PatParsing() {




        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("curie").child("patients");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int check =0;
                for(int i=0;i<dataSnapshot.getChildrenCount();i++) {


                    Patient patient = new Patient();


    patient.setDoctorid(dataSnapshot.child(i + "").child("doctorid").getValue().toString());
    patient.setHeight(dataSnapshot.child(i + "").child("height").getValue().toString());
    patient.setHospital(dataSnapshot.child(i + "").child("hospital").getValue().toString());
    patient.setName(dataSnapshot.child(i + "").child("name").getValue().toString());
    patient.setPid(dataSnapshot.child(i + "").child("pid").getValue().toString());
    patient.setNurseid(dataSnapshot.child(i + "").child("nurseid").getValue().toString());
    patient.setWeight(dataSnapshot.child(i + "").child("weight").getValue().toString());
    patient.setTemperature(dataSnapshot.child(i + "").child("temperature").getValue().toString());
    patient.setStatus(dataSnapshot.child(i + "").child("status").getValue().toString());
    Log.d(TAG, "onDataChange: "+dataSnapshot.child(i + "").child("doctorid").getValue().toString() + dataSnapshot.child(i + "").child("height").getValue().toString()
    +dataSnapshot.child(i + "").child("name").getValue().toString());
    patData.add(patient);

                }

                pat_count.setText(patData.size()+" Patients");
                final PatientAdapter adapter = new PatientAdapter(patData);
                pat_recyclerView.setAdapter(adapter);
                dialog.cancel();
                adapter.setListener(new PatientAdapter.RecyclerViewListener() {
                    @Override
                    public void itemselected(int position) {



                        Patient patient = patData.get(position);
                        PreferenceManager.saveCurrentPatient(Nurse.this,patient);
                        Intent i = new Intent(Nurse.this, PatientDatails.class);

                        startActivity(i);

                    }
                });
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "No Internet Connection", error.toException());
            }
        });






    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
