package com.example.curie;

import android.content.Context;

import com.example.curie.model.Patient;
import com.example.curie.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class PreferenceManager {

    public static final String PREF_NAME = "userdata";
    public static final String USER_KEY = "user_key";
    public static final String PATIENT_KEY = "user_key";


    public static void saveCurrentUser(Context context, User modal){
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit().putString(USER_KEY,new Gson().toJson(modal)).apply();
    }

    public static User getCurrentUser(Context context){
        String savedData = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE).getString(USER_KEY,"");
        User modal = new Gson().fromJson(savedData, new TypeToken<User>(){}.getType());
        if (modal != null){
            return modal;
        }
        return null;
    }


    public static void saveCurrentPatient(Context context, Patient modal){
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit().putString(PATIENT_KEY,new Gson().toJson(modal)).apply();
    }

    public static Patient getCurrentPatient(Context context){
        String savedData = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE).getString(PATIENT_KEY,"");
        Patient modal = new Gson().fromJson(savedData, new TypeToken<Patient>(){}.getType());
        if (modal != null){
            return modal;
        }
        return null;
    }
}
