package com.example.curie.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Patient {

    @SerializedName("doctorid")
    @Expose
    private String doctorid;
    @SerializedName("status")
    @Expose
    private String status;


    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("hospital")
    @Expose
    private String hospital;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("nurseid")
    @Expose
    private String nurseid;
    @SerializedName("pid")
    @Expose
    private String pid;
    @SerializedName("reason")
    @Expose
    private String reason;
    @SerializedName("temperature")
    @Expose
    private String temperature;
    @SerializedName("weight")
    @Expose
    private String weight;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDoctorid() {
        return doctorid;
    }

    public void setDoctorid(String doctorid) {
        this.doctorid = doctorid;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNurseid() {
        return nurseid;
    }

    public void setNurseid(String nurseid) {
        this.nurseid = nurseid;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

}
