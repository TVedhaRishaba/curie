package com.example.curie.model;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Curie {
    @SerializedName("patients")
    @Expose
    private List<Patient> patients = null;
    @SerializedName("users")
    @Expose
    private List<User> users = null;

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
