package com.example.curie.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class root {
    @SerializedName("curie")
    @Expose
    private Curie curie;

    public Curie getCurie() {
        return curie;
    }

    public void setCurie(Curie curie) {
        this.curie = curie;
    }
}
