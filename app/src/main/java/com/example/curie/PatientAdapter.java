package com.example.curie;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.curie.model.Patient;

import java.util.List;

/**
 * Created by Rishaba on 12/28/2017.
 */

public class PatientAdapter extends RecyclerView.Adapter<PatientAdapter.Holder>{

    List<Patient> data;
    LayoutInflater inflater;
    RecyclerViewListener listener;

    public PatientAdapter(List<Patient> data){
        this.data = data;
    }

    @Override
    public PatientAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        return new PatientAdapter.Holder(inflater.inflate(R.layout.patient_view_item,parent,false));

    }
    public void setListener(RecyclerViewListener listener) {
        this.listener = listener;
    }
    @Override
    public void onBindViewHolder(Holder holder, int position) {

        String name = data.get(position).getName();
        String reason = data.get(position).getReason();
        String status = data.get(position).getStatus();

        holder.pat_name.setText(name);
        holder.pat_reason.setText(reason);
        holder.pat_status.setText(status);

      holder.count.setText((position+1)+"");
     }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView pat_name,pat_reason,pat_status,count;

        public Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null){

                        listener.itemselected(getAdapterPosition());
                    }
                }
            });

            pat_reason =(TextView)itemView.findViewById(R.id.pat_reason);
            pat_status =(TextView)itemView.findViewById(R.id.pat_status);
            pat_name =(TextView)itemView.findViewById(R.id.pat_name);
            count =(TextView)itemView.findViewById(R.id.count);

        }

    }
    interface RecyclerViewListener {
        public void itemselected(int position);


    }


}
