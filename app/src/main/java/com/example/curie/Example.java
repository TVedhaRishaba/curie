import android.support.v4.app.Fragment;

//package com.example.curie;
//
//import android.app.DatePickerDialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.app.AlertDialog;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.inputmethod.EditorInfo;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.aigestudio.wheelpicker.WheelPicker;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import systems.ellora.curie.R;
//import systems.ellora.curie.api.Externals.MultiStateToggleButton;
//import systems.ellora.curie.api.Externals.ScrollingValuePicker;
//import systems.ellora.curie.api.Externals.SeekArc;
//import systems.ellora.curie.api.Model.WaitingRoom.CreateApponitmentModel;
//import systems.ellora.curie.api.Model.WaitingRoom.Encounter;
//import systems.ellora.curie.api.Model.WaitingRoom.Invoice;
//import systems.ellora.curie.api.Model.WaitingRoom.Patient;
//import systems.ellora.curie.api.Model.WaitingRoom.Vitals;
//import systems.ellora.curie.api.Remote.NetworkManager;
//import systems.ellora.curie.api.Remote.VolleyNetwork;
//import systems.ellora.curie.api.UserManagement.UserManager;
//import systems.ellora.curie.api.Utils.Helpers.DateHelper;
//import systems.ellora.curie.doctor.ExternalUI.Model.Appointment;
//import systems.ellora.curie.waiting_room.EncounterStatus;
//import systems.ellora.curie.waiting_room.FirebaseManager;
//import systems.ellora.curie.waiting_room.FragmentAdapterCommunicator;
//
//import static systems.ellora.curie.waiting_room.EncounterStatus.getStatusFromString;
//import static systems.ellora.curie.waiting_room.EncounterStatus.getStatusString;
//import static systems.ellora.curie.waiting_room.FirebaseManager.updateEncounter;
//
///**
// * Created by Balasubramaniyam on 9/21/2017.
// */
//
//public class PediatricianView extends Fragment{
//
//
//    private boolean lockScrollers = false;
//    public static int itemPosition =0;
//    // WaitingRoomListItemModel model;
//    //WaitingRoomManager v;
//    MultiStateToggleButton heart_beat_selection,waiting_room_vitals_respiratory_rate;
//    final Handler handler = new Handler();
//    final Handler mHeightInactivityHandler = new Handler(  );
//    final Handler mHeadInactivityHandler = new Handler(  );
//    final Handler mSystolicInactivityHandler = new Handler(  );
//    final Handler mDiastolicInactivityHandler = new Handler(  );
//    final Handler mDefaultInactivityHandler = new Handler(  );
//    final Handler mHeartBeatHandler = new Handler(  ),
//            mRespRateHandler = new Handler(  );
//
//    final Handler mTemperatureHandler = new Handler(  );
//    SeekArc temperature;
//    TextView temperature_text;
//    String date;
//    WheelPicker heightWheel, headWheel;
//    CheckBox newPatient_box;
//    private String firebaseKey;
//    Encounter encounter;
//    LinearLayout vitals_control_layer,wheel_protector_height,temperature_protector,wheel_protector_head
//            ,systolic_protector, diastolic_protector, heart_rate_protector,resp_rate_protector;
//
//
//    private Button confirm_button,no_vitals_button;
//    FragmentAdapterCommunicator adapterCommunicator;
//
//
//    EditText weight_input;
//
//    ScrollingValuePicker systolic_picker,diastolic_picker;
//    TextView waiting_room_detailed_gender_age,waiting_room_detailed_phone,waiting_room_detailed_name,waiting_room_detailed_reason;
//
//    public static final String TAG = "WaitingRoomVitals";
//
//    public void setAdapterCommunicator(FragmentAdapterCommunicator adapterCommunicator) {
//        this.adapterCommunicator = adapterCommunicator;
//    }
//
//    public static PediatricianView newInstance(int pos, String firebaseKey, String date) {
//        PediatricianView fragment = new PediatricianView();
//        Bundle args = new Bundle();
//        args.putInt( "position", pos );
//        args.putString( "f-key",firebaseKey );
//        args.putString( "date", date );
//        fragment.setArguments( args );
//        return fragment;
//    }
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate( savedInstanceState );
//        if (getArguments() != null) {
//            itemPosition = getArguments().getInt( "position" );
//            firebaseKey  = getArguments().getString( "f-key" );
//            date = getArguments().getString( "date" );
//        }
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        // WaitingRoomManager v = new WaitingRoomManager( getActivity() );
//        // model = v.getByPos( itemPosition );
//        encounter = FirebaseManager.getEncounterByFID( firebaseKey );
//        View activity = inflater.inflate( R.layout.pediatrician_vitals_fragment, container, false );
//
//        waiting_room_detailed_gender_age = (TextView) activity.findViewById( R.id.waiting_room_detailed_gender_age );
//        waiting_room_detailed_phone = (TextView) activity.findViewById( R.id.waiting_room_detailed_phone );
//        waiting_room_detailed_name = (TextView) activity.findViewById( R.id.waiting_room_detailed_name );
//        waiting_room_detailed_reason = (TextView) activity.findViewById( R.id.waiting_room_detailed_reason );
//        systolic_picker = (ScrollingValuePicker) activity.findViewById( R.id.systolic_scroller );
//        diastolic_picker = (ScrollingValuePicker) activity.findViewById( R.id.diastolic_scroller );
//        heart_beat_selection = (MultiStateToggleButton) activity.findViewById( R.id.waiting_room_vitals_heart_rate );
//        waiting_room_vitals_respiratory_rate = (MultiStateToggleButton) activity.findViewById( R.id.waiting_room_vitals_respiratory_rate );
//        temperature_text = (TextView) activity.findViewById( R.id.temperature_text );
//        temperature = (SeekArc) activity.findViewById( R.id.seekArc );
//        weight_input = (EditText) activity.findViewById( R.id.weight_input );
//        heightWheel = (WheelPicker) activity.findViewById(R.id.wheel);
//        headWheel = (WheelPicker) activity.findViewById(R.id.head_circumstance);
//
//        newPatient_box = (CheckBox) activity.findViewById( R.id.waiting_room_vitals_new_patient );
//        confirm_button = (Button) activity.findViewById( R.id.confirm_button );
//        no_vitals_button  = (Button) activity.findViewById( R.id.no_vitals_button );
//        vitals_control_layer = (LinearLayout) activity.findViewById( R.id.vitals_control_layer );
//
//        initProtectors(activity);
//
//        vitals_control_layer.setVisibility( View.GONE );
//        return activity;
//    }
//
//    private void initProtectors(View activity) {
//
//        temperature_protector = (LinearLayout) activity.findViewById( R.id.temperature_protector );
//        temperature_protector.setOnClickListener( new LockClickListener() );
//        temperature_protector.setOnLongClickListener( new LockLongClickListener() );
//
//        wheel_protector_height = (LinearLayout) activity.findViewById( R.id.wheel_protector_height );
//        wheel_protector_height.setOnClickListener( new LockClickListener() );
//        wheel_protector_height.setOnLongClickListener( new LockLongClickListener() );
//
//        /*wheel_protector_weight = (LinearLayout) activity.findViewById( R.id.wheel_protector_weight );
//        wheel_protector_weight.setOnClickListener( new PediatricianView.LockClickListener() );
//        wheel_protector_weight.setOnLongClickListener( new PediatricianView.LockLongClickListener() );
//        */
//        wheel_protector_head = (LinearLayout) activity.findViewById( R.id.wheel_protector_head );
//        wheel_protector_head.setOnClickListener( new LockClickListener() );
//        wheel_protector_head.setOnLongClickListener( new LockLongClickListener() );
//
//        systolic_protector = (LinearLayout) activity.findViewById( R.id.systolic_protector );
//        systolic_protector.setOnClickListener( new LockClickListener() );
//        systolic_protector.setOnLongClickListener( new LockLongClickListener() );
//
//        diastolic_protector = (LinearLayout) activity.findViewById( R.id.diastolic_protector );
//        diastolic_protector.setOnClickListener( new LockClickListener() );
//        diastolic_protector.setOnLongClickListener( new LockLongClickListener() );
//
//        heart_rate_protector = (LinearLayout) activity.findViewById( R.id.heart_rate_protector );
//        heart_rate_protector.setOnClickListener( new LockClickListener() );
//        heart_rate_protector.setOnLongClickListener( new LockLongClickListener() );
//
//        resp_rate_protector = (LinearLayout) activity.findViewById( R.id.resp_rate_protector );
//        resp_rate_protector.setOnClickListener( new LockClickListener() );
//        resp_rate_protector.setOnLongClickListener( new LockLongClickListener() );
//    }
//
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated( savedInstanceState );
//
//        handler.post(new Runnable(){
//            @Override
//            public void run(){
//                waiting_room_detailed_gender_age.setText( encounter.getPatient().getSex()+", "+ encounter.getPatient().getAge()+" Yrs" );
//                waiting_room_detailed_gender_age.setVisibility( View.GONE );
//                waiting_room_detailed_phone.setText( "Phone :"+encounter.getPatient().getPhoneNumber() );
//                waiting_room_detailed_name.setText( encounter.getPatient().getName()+" / " +encounter.getPatient().getSex().charAt( 0 )+" / "+encounter.getPatient().getAge());
//                waiting_room_detailed_reason.setText( "Reason : "+encounter.getPatient().getReason() );
//            }
//        });
//
//        setupTemperature();
//        setupRulers();
//        setupSystolicAndDiastolic();
//        setupHeartBeat();
//        setupMisc();
//        initButton();
//        setStoredValues();
//        if (EncounterStatus.getStatusFromString( encounter.getStatus() ) == EncounterStatus.TRIAGED ||EncounterStatus.getStatusFromString( encounter.getStatus() ) == EncounterStatus.IN_PROGRESS ){
//            temperature_protector.setVisibility( View.VISIBLE );
//            wheel_protector_height.setVisibility( View.VISIBLE );
//            wheel_protector_head.setVisibility( View.VISIBLE );
//            systolic_protector.setVisibility( View.VISIBLE );
//            diastolic_protector.setVisibility( View.VISIBLE );
//            heart_rate_protector.setVisibility( View.VISIBLE );
//            resp_rate_protector.setVisibility( View.VISIBLE );
//        }else{
//            temperature_protector.setVisibility( View.GONE );
//            wheel_protector_height.setVisibility( View.GONE );
//            wheel_protector_head.setVisibility( View.GONE );
//            systolic_protector.setVisibility( View.GONE );
//            diastolic_protector.setVisibility( View.GONE );
//            heart_rate_protector.setVisibility( View.GONE );
//            resp_rate_protector.setVisibility( View.GONE );
//        }
//    }
//
//
//    private void initButton() {
//
//
//        if (!DateHelper.getDateInfo( Calendar.getInstance() ).equals( date )){
//            no_vitals_button.setEnabled( false );
//            confirm_button.setEnabled( false );
//            disableEverything();
//        }else{
//            if (encounter != null && encounter.getStatus() != null){
//                switch (getStatusFromString( encounter.getStatus() )){
//                    case PLANNED:
//                        no_vitals_button.setSelected( false );
//                        confirm_button.setSelected( false );
//                        break;
//                    case ARRIVED:
//
//                        no_vitals_button.setSelected( true );
//                        confirm_button.setSelected( true );
//                        confirm_button.setOnClickListener( new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                AlertDialog dialog = null;
//                                AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
//                                builder.setMessage( "Confirm vitals ?" );
//                                builder.setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        dialogInterface.dismiss();
//                                    }
//                                } ).setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        dialogInterface.dismiss();
//                                        encounter.getObservation().setNotPerformed( false );
//                                        encounter.setStatus( getStatusString( EncounterStatus.TRIAGED ) );
//                                        updateEncounter( encounter );
//                                    }
//                                } );
//                                dialog = builder.create();
//                                dialog.setCancelable( true );
//                                dialog.show();
//
//
//                            }
//                        } );
//
//                        no_vitals_button.setOnClickListener( new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//
//                                AlertDialog dialog = null;
//                                AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
//                                builder.setMessage( "Are you sure, no vitals required ?" );
//                                builder.setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        dialogInterface.dismiss();
//                                    }
//                                } ).setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        dialogInterface.dismiss();
//                                        encounter.getObservation().setNotPerformed( true );
//                                        encounter.setStatus( getStatusString( EncounterStatus.TRIAGED ) );
//                                        updateEncounter( encounter );
//                                    }
//                                } );
//
//                                dialog = builder.create();
//                                dialog.setCancelable( true );
//                                dialog.show();
//
//
//
//                            }
//                        } );
//                        break;
//                    case TRIAGED:
//                        confirm_button.setText( "VITALS TAKEN" );
//                        disableEverything();
//                        break;
//                    case WAITING:
//                        confirm_button.setText( "VITALS TAKEN" );
//                        disableEverything();
//
//                        no_vitals_button.setSelected( true );
//                        confirm_button.setSelected( true );
//                    case IN_PROGRESS:
//                        confirm_button.setText( "VITALS TAKEN" );
//                        disableEverything();
//                        no_vitals_button.setSelected( true );
//                        confirm_button.setSelected( true );
//                        break;
//                    case FINISHED:
//                        confirm_button.setText( "VITALS TAKEN" );
//                        disableEverything();
//                        no_vitals_button.setEnabled( false );
//                        break;
//                    case BILLED:
//                        confirm_button.setText( "VITALS TAKEN" );
//                        disableEverything();
//                        no_vitals_button.setEnabled( false );
//                        break;
//                    case CANCELLED:
//                        break;
//                }
//            }
//        }
//
//
//    }
//
//    private void disableEverything() {
//
//        vitals_control_layer.setVisibility( View.VISIBLE );
//
//        if (encounter.getObservation().isNotPerformed()){
//            confirm_button.setSelected( false );
//            no_vitals_button.setSelected( true );
//
//        }else{
//
//            confirm_button.setSelected( true );
//            no_vitals_button.setSelected( false );
//            no_vitals_button.setEnabled( false );
//
//        }
//    }
//
//    private void setupSystolicAndDiastolic() {
//
//        systolic_picker.setMaxValue(90, 160);
//        systolic_picker.setMediumValue(120, 139);
//        systolic_picker.setInitValue( encounter.getObservation().getVitals().getSystolicBp() );
//        systolic_picker.setValueTypeMultiple(5);
//        systolic_picker.getScrollView().setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    systolic_picker.getScrollView().startScrollerTask();
//                }
//                return false;
//            }
//        });
//
//        systolic_picker.setListener( new ScrollingValuePicker.onScrollValueChangeListener() {
//            @Override
//            public void onValueChanged(final int newPosition) {
//                ///////////////////////////////////////////////////////////////
//                encounter.getObservation().getVitals().setSystolicBp( newPosition );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        systolic_protector.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mSystolicInactivityHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mSystolicInactivityHandler.postDelayed( runnable, 2500 );
//                ///////////////////////////////////////////////////////////////
//            }
//        } );
//
//
//        diastolic_picker.setMaxValue(50, 120);
//        diastolic_picker.setMediumValue(50, 80);
//        diastolic_picker.setInitValue( encounter.getObservation().getVitals().getDiastolicBp() );
//        diastolic_picker.setValueTypeMultiple(5);
//        diastolic_picker.getScrollView().setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    diastolic_picker.getScrollView().startScrollerTask();
//                }
//                return false;
//            }
//        });
//
//        diastolic_picker.setListener( new ScrollingValuePicker.onScrollValueChangeListener() {
//            @Override
//            public void onValueChanged(final int newPosition) {
//                ///////////////////////////////////////////////////////////////
//                encounter.getObservation().getVitals().setDiastolicBp( newPosition );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        diastolic_protector.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mDiastolicInactivityHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mDiastolicInactivityHandler.postDelayed( runnable, 2500 );
//                ///////////////////////////////////////////////////////////////
//
//            }
//        } );
//    }
//
//    private void setupMisc() {
//
//        newPatient_box.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                encounter.setNewPatient( b );
//                updateVitals( encounter );
//            }
//        } );
//
//    }
//
//    private void setStoredValues() {
//
//        try{
//            final Vitals vitals = encounter.getObservation().getVitals();
//            int height = (int) vitals.getHeight();
//            int headCircumference = (int) vitals.getHeadCircumference();
//
//            weight_input.setText( ""+vitals.getWeight() );
//            String gender = encounter.getPatient().getSex();
//            int age = encounter.getPatient().getAge();
//            if (height == 0){
//                AgeProcessor process;
//                if (gender.toUpperCase().equals( "MALE" ))
//                    process = new AgeProcessor( age, AgeProcessor.Gender.Male );
//                else
//                    process = new AgeProcessor( age, AgeProcessor.Gender.Female );
//                height = process.getApproximateHeight() - 50;
//            }
//
//            final boolean[] heart_rate_states = new boolean[3];
//            for (int i=0; i<3; i++){
//                if (i == vitals.getHeartRate()){
//                    heart_rate_states[i] = true;
//                }else{
//                    heart_rate_states[i] = false;
//                }
//            }
//
//
//            final boolean[] respiratory_rate_states = new boolean[2];
//            for (int i=0; i<2; i++){
//                if (i == vitals.getRespiratoryRate()){
//                    respiratory_rate_states[i] = true;
//                }else{
//                    respiratory_rate_states[i] = false;
//                }
//            }
//
//            //newPatient_box.setChecked(  );
//
//            final int finalHeight = height;
//
//            final int headCircumference1 = headCircumference;
//            handler.post( new Runnable(){
//                @Override
//                public void run(){
//                    heightWheel.setSelectedItemPosition( finalHeight );
//                    //weightWheel.setSelectedItemPosition( finalWeight );
//                    headWheel.setSelectedItemPosition( headCircumference1 );
//                    temperature.setProgress( vitals.getBodyTemp() );
//                    waiting_room_vitals_respiratory_rate.setStates( respiratory_rate_states );
//                    heart_beat_selection.setStates( heart_rate_states );
//                }
//            });
//            final TextView amount = (TextView) getActivity().findViewById( R.id.amount_text );
//            Button payButton = (Button) getActivity().findViewById( R.id.pay_button );
//
//            final Button follow_up_button = (Button) getActivity().findViewById( R.id.follow_up_button );
//            LinearLayout billing_session_root = (LinearLayout) getActivity().findViewById( R.id.billing_session_root );
//            LinearLayout vitals_section_root = (LinearLayout) getActivity().findViewById( R.id.vitals_section_root );
//
//            final String ripple_symbol = "\u20B9";
//            vitals_section_root.setVisibility( View.VISIBLE );
//            billing_session_root.setVisibility( View.GONE );
//
//            if (getStatusFromString( encounter.getStatus() ) == EncounterStatus.FINISHED){
//                final EditText pharmacy_amount = (EditText) getActivity().findViewById( R.id.pharmacy_amount );
//                pharmacy_amount.addTextChangedListener( new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable editable) {
//                        try{
//                            if (!editable.toString().equals( "" )){
//                                int pharmacyAmount = Integer.parseInt( pharmacy_amount.getText().toString() );
//                                amount.setText( ripple_symbol+" "+(pharmacyAmount  + encounter.getInvoice().getTotal()));
//                            }
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//                    }
//                } );
//                pharmacy_amount.setOnFocusChangeListener( new View.OnFocusChangeListener() {
//                    @Override
//                    public void onFocusChange(View view, boolean b) {
//                        try{
//                            if (!pharmacy_amount.getText().toString().equals( "" )){
//                                int pharmacyAmount = Integer.parseInt( pharmacy_amount.getText().toString() );
//                                amount.setText( ripple_symbol+" "+(pharmacyAmount  + encounter.getInvoice().getTotal()));
//                            }
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//
//                    }
//                } );
//
//                amount.setText( ripple_symbol +" "+encounter.getInvoice().getTotal() );
//                payButton.setOnClickListener( new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if (encounter.getObservation().isFollowUp()){
//
//                            AlertDialog.Builder builder  = new AlertDialog.Builder( getActivity() );
//                            builder.setMessage( "Have you discussed follow-up with patient ?" );
//                            AlertDialog dialog = builder.setNegativeButton( "Cancel", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    dialogInterface.dismiss();
//                                }
//                            } ).setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    try{
//                                        if (!pharmacy_amount.getText().toString().equals( "" )){
//                                            int pharmacyAmount = Integer.parseInt( pharmacy_amount.getText().toString() );
//                                            int total = pharmacyAmount  + encounter.getInvoice().getTotal();
//                                            encounter.getInvoice().setTotal( total );
//                                        }
//                                    }catch (Exception e){
//                                        e.printStackTrace();
//                                    }
//                                    encounter.setStatus( getStatusString( EncounterStatus.BILLED ) );
//                                    updateVitals( encounter );
//                                    adapterCommunicator.clickNextItem(itemPosition);
//                                }
//                            } ).create();
//
//                            dialog.show();
//                        }else{
//                            encounter.getInvoice().setPaid( true );
//                            try{
//                                if (!pharmacy_amount.getText().toString().equals( "" )){
//                                    int pharmacyAmount = Integer.parseInt( pharmacy_amount.getText().toString() );
//                                    int total = pharmacyAmount  + encounter.getInvoice().getTotal();
//                                    encounter.getInvoice().setTotal( total );
//                                }
//                            }catch (Exception e){
//                                e.printStackTrace();
//                            }
//                            encounter.setStatus( getStatusString( EncounterStatus.BILLED ) );
//                            updateVitals( encounter );
//                            adapterCommunicator.clickNextItem(itemPosition);
//                        }
//
//                    }
//                } );
//                if (encounter.getObservation().isFollowUp()){
//                    follow_up_button.setVisibility( View.VISIBLE );
//                    follow_up_button.setOnClickListener( new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            Calendar myCalendar = Calendar.getInstance();
//
//                            DatePickerDialog datePickerDialog  = new DatePickerDialog( getActivity(), new DatePickerDialog.OnDateSetListener() {
//                                @Override
//                                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
//                                    //show the popup
//                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//                                    try {
//                                        Date date = simpleDateFormat.parse( (i+"-"+(i1+1)+"-"+i2) );
//                                        final FindSlotDialog dialog = FindSlotDialog.getInstance(simpleDateFormat.format( date ) );
//                                        dialog.setSlotSelectedListener( new SlotListAdapter.onSlotSelectedListener() {
//                                            @Override
//                                            public void onSlotSelected(Appointment appointment) {
//                                                dialog.dismiss();
//                                                //create appointment request here
//                                                final ProgressDialog appointmentAddingDialog = new ProgressDialog( getActivity() );
//                                                appointmentAddingDialog.setMessage( "Creating Appointment..." );
//                                                appointmentAddingDialog.setCancelable( false );
//                                                appointmentAddingDialog.show();
//                                                CreateApponitmentModel model = new CreateApponitmentModel();
//
//                                                Patient patient = encounter.getPatient();
//                                                model.setSlotId( appointment.getId() );
//                                                model.setAge( patient.getAge() );
//                                                model.setCity( patient.getAddress() );
//                                                model.setPatientId( patient.getId() );
//                                                model.setCreated( DateHelper.getDateInfo( Calendar.getInstance() ));
//                                                try{
//                                                    model.setDoctorId( UserManager.getLoginUserInfo( getActivity() ).getDoctorId() );
//                                                    model.setFacilityId( UserManager.getLoginUserInfo( getActivity() ).getFacilityId() );
//                                                }catch (Exception e){
//                                                    e.printStackTrace();
//                                                }
//                                                model.setGender( patient.getSex() );
//                                                model.setName( patient.getName() );
//                                                model.setPhone( patient.getPhoneNumber() );
//                                                model.setReason( patient.getReason() );
//                                                NetworkManager.createAppointment( model, new VolleyNetwork.OnResultReturns() {
//                                                    @Override
//                                                    public void onSuccess(String response) {
//                                                        Toast.makeText( getActivity(),"Appointment Booked", Toast.LENGTH_SHORT ).show();
//                                                        if (appointmentAddingDialog.isShowing())appointmentAddingDialog.dismiss();
//                                                    }
//
//                                                    @Override
//                                                    public void onFailed(String error) {
//                                                        Toast.makeText( getActivity(),"Unable to Book", Toast.LENGTH_SHORT ).show();
//                                                        if (appointmentAddingDialog.isShowing())appointmentAddingDialog.dismiss();
//                                                    }
//                                                } );
//
//                                            }
//                                        } );
//                                        dialog.show( getActivity().getSupportFragmentManager(),"slots" );
//                                    } catch (ParseException e) {
//                                        e.printStackTrace();
//                                        Toast.makeText( getActivity(),"Unable to load slot...", Toast.LENGTH_SHORT ).show();
//                                    }
//
//
//                                }
//                            }, myCalendar
//                                    .get( Calendar.YEAR ), myCalendar.get( Calendar.MONTH ),
//                                    myCalendar.get( Calendar.DAY_OF_MONTH ) );
//                            Calendar cal = Calendar.getInstance();
//                            cal.add(Calendar.DAY_OF_YEAR, 1);
//                            datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
//                            datePickerDialog.setTitle( "" );
//                            datePickerDialog.show();
//                        }
//                    } );
//                }else{
//                    follow_up_button.setVisibility( View.GONE );
//                }
//
//                vitals_section_root.setVisibility( View.GONE );
//                billing_session_root.setVisibility( View.VISIBLE );
//
//
//            }
//        }catch (Exception e){
//            //exception throwed when we reach the last item
//        }
//
//    }
//
//
//    private void setupHeartBeat() {
//
//        heart_beat_selection.setListener( new MultiStateToggleButton.ToggleListener() {
//            @Override
//            public void itemSelected(final int position) {
//                ///////////////////////////////////////////////////////////////
//
//                encounter.getObservation().getVitals().setHeartRate( position );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        heart_rate_protector.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mHeartBeatHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mHeartBeatHandler.postDelayed( runnable, 2500 );
//                ///////////////////////////////////////////////////////////////
//
//
//
//            }
//        } );
//
//        waiting_room_vitals_respiratory_rate.setListener( new MultiStateToggleButton.ToggleListener() {
//            @Override
//            public void itemSelected(final int position) {
//
//                encounter.getObservation().getVitals().setRespiratoryRate( position );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        resp_rate_protector.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mRespRateHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mRespRateHandler.postDelayed( runnable, 2500 );
//
//            }
//        } );
//
//    }
//
//    private void setupRulers() {
//
//        new Thread(new Runnable(){
//            @Override
//            public void run(){
//                final List<String> data = new ArrayList<>();
//                for (int i = 50; i < 200; i++)
//                    data.add(i+" cm");
//
//                final List<String> data1 = new ArrayList<>();
//                for (int i = 3; i < 140; i++)
//                    data1.add(i+ " kg");
//                final List<String> data2 = new ArrayList<>();
//                for (int i = 20; i < 100; i++)
//                    data2.add(i+ " cm");
//                handler.post(new Runnable(){
//                    @Override
//                    public void run(){
//                        heightWheel.setData(data);
//
//                        headWheel.setData( data2 );
//                        setStoredValues();
//                    }
//                });
//            }
//        }).start();
//       /* weightWheel.setOnItemSelectedListener( new WheelPicker.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(WheelPicker picker, Object data, final int position) {
//                ///////////////////////////////////////////////////////////////
//
//                encounter.getObservation().getVitals().setWeight( position );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        wheel_protector_weight.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mWeightInactivityHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mWeightInactivityHandler.postDelayed( runnable, 2500 );
//                ///////////////////////////////////////////////////////////////
//            }
//        } );*/
//
//
//        weight_input.setOnEditorActionListener(
//                new EditText.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
//                                actionId == EditorInfo.IME_ACTION_DONE ||
//                                event.getAction() == KeyEvent.ACTION_DOWN &&
//                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                            try{
//                                if (Double.parseDouble( weight_input.getText().toString()) != 0){
//                                    encounter.getObservation().getVitals().setWeight( Double.parseDouble( weight_input.getText().toString()) );
//                                    updateVitals( encounter );
//                                }
//                                View view = getActivity().getCurrentFocus();
//
//                                if (view != null) {
//                                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService( Context.INPUT_METHOD_SERVICE);
//                                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                                }
//                                weight_input.clearFocus();
//
//                            }catch (Exception e){
//                                e.printStackTrace();
//                            }
//
//                            return true; // consume.
//                        }
//                        return false; // pass on to other listeners.
//                    }
//                });
//        heightWheel.setOnItemSelectedListener( new WheelPicker.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(WheelPicker picker, Object data, final int position) {
//                ///////////////////////////////////////////////////////////////
//                encounter.getObservation().getVitals().setHeight( position );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        wheel_protector_height.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mHeightInactivityHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mHeightInactivityHandler.postDelayed( runnable, 2500 );
//                ///////////////////////////////////////////////////////////////
//            }
//        } );
//
//        headWheel.setOnItemSelectedListener( new WheelPicker.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(WheelPicker picker, Object data, final int position) {
//                ///////////////////////////////////////////////////////////////
//
//                encounter.getObservation().getVitals().setHeadCircumference( position );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        wheel_protector_head.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mHeadInactivityHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mHeadInactivityHandler.postDelayed( runnable, 2500 );
//                ///////////////////////////////////////////////////////////////
//            }
//        } );
//
//    }
//
//
//    private void setupTemperature() {
//     /*
//        temperature.setProgressColor( 21 );
//        temperature.setArcColor( 21 );
//        temperature.setMax(  );*/
//
//
//        temperature.setOnSeekArcChangeListener( new SeekArc.OnSeekArcChangeListener() {
//            @Override
//            public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
//                final int binN = Math.abs((progress / 10));
//                final int smlN = Math.abs( ( binN * 10) - progress );
//                temperature_text.setText(""+(binN+95)+"."+smlN );
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekArc seekArc) {
//                try{
//                    mTemperatureHandler.removeCallbacksAndMessages(null);
//                    mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                }catch (Exception e){
//
//                }
//
//            }
//
//            @Override
//            public void onStopTrackingTouch(final SeekArc seekArc) {
//                encounter.getObservation().getVitals().setBodyTemp( seekArc.getProgress() );
//                Runnable runnable = new Runnable() {
//                    @Override
//                    public void run() {
//                        temperature_protector.setVisibility( View.VISIBLE );
//                        updateVitals( encounter );
//                    }
//                };
//                mTemperatureHandler.removeCallbacksAndMessages(null);
//                mDefaultInactivityHandler.removeCallbacksAndMessages( null );
//                mTemperatureHandler.postDelayed( runnable, 2500 );
//
//            }
//        } );
//
//    }
//
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//    }
//
//    public void dataChanged() {
//        Log.d( TAG, "dataChanged: data changed" );
//        encounter = FirebaseManager.getEncounterByFID( firebaseKey );
//        if (encounter != null){
//            setStoredValues();
//            initButton();
//        }
//
//    }
//
//
//
//    private class LockClickListener implements View.OnClickListener {
//        @Override
//        public void onClick(View view) {
//            Toast.makeText( getContext(), "Long press to unlock...",Toast.LENGTH_SHORT ).show();
//        }
//    }
//
//    private class LockLongClickListener implements View.OnLongClickListener {
//        @Override
//        public boolean onLongClick(View view) {
//            view.setClickable( false );
//            view.setVisibility( View.GONE );
//
//            switch (view.getId()){
//                case R.id.temperature_protector:
//                    mDefaultInactivityHandler.postDelayed( new Runnable() {
//                        @Override
//                        public void run() {
//                            temperature_protector.setVisibility( View.VISIBLE );
//                        }
//                    } ,5000);
//
//                    break;
//                case R.id.wheel_protector_height:
//
//                    mDefaultInactivityHandler.postDelayed( new Runnable() {
//                        @Override
//                        public void run() {
//                            wheel_protector_height.setVisibility( View.VISIBLE );
//                        }
//                    } ,5000);
//
//                    break;
//                case R.id.wheel_protector_head:
//                    mDefaultInactivityHandler.postDelayed( new Runnable() {
//                        @Override
//                        public void run() {
//                            wheel_protector_head.setVisibility( View.VISIBLE );
//                        }
//                    } ,5000);
//
//                    break;
//                case R.id.systolic_protector:
//                    mDefaultInactivityHandler.postDelayed( new Runnable() {
//                        @Override
//                        public void run() {
//                            systolic_protector.setVisibility( View.VISIBLE );
//                        }
//                    } ,5000);
//
//                    break;
//                case R.id.diastolic_protector:
//                    mDefaultInactivityHandler.postDelayed( new Runnable() {
//                        @Override
//                        public void run() {
//                            diastolic_protector.setVisibility( View.VISIBLE );
//                        }
//                    } ,5000);
//
//                    break;
//                case R.id.heart_rate_protector:
//                    mDefaultInactivityHandler.postDelayed( new Runnable() {
//                        @Override
//                        public void run() {
//                            heart_rate_protector.setVisibility( View.VISIBLE );
//                        }
//                    } ,5000);
//
//                    break;
//                case R.id.resp_rate_protector:
//                    mDefaultInactivityHandler.postDelayed( new Runnable() {
//                        @Override
//                        public void run() {
//                            resp_rate_protector.setVisibility( View.VISIBLE );
//                        }
//                    } ,5000);
//
//                    break;
//            }
//            return true;
//        }
//    }
//
//
//    private void updateVitals(Encounter encounter){
//        encounter.getObservation().setNotPerformed( false );
//        updateEncounter( encounter );
//    }
//}
//
public class Example extends Fragment {

}