package com.example.curie;


public enum  EncounterStatus {
    PLANNED,ARRIVED,  IN_PROGRESS,WAITING, FINISHED, BILLED, CANCELLED;

    public static String getStatusString(EncounterStatus status){
        switch (status){
            case PLANNED:
                return "Planned";
            case ARRIVED:
                return "Arrived";
            case WAITING:
                return "Waiting";
            case IN_PROGRESS:
                return "In Progress";
            case FINISHED:
                return "Finished";
            case BILLED:
                return "Billed";
            case CANCELLED:
                return "Cancelled";
            default:
                return "UnSpecified";
        }
    }
    public static EncounterStatus getStatusFromString(String status){
        switch (status.toUpperCase()){
            case "PLANNED":
                return EncounterStatus.PLANNED;
            case "ARRIVED":
                return EncounterStatus.ARRIVED;

            case "IN PROGRESS":
                return EncounterStatus.IN_PROGRESS;
            case "INPROGRESS":
                return EncounterStatus.IN_PROGRESS;
            case "WAITING":
                return EncounterStatus.WAITING;
            case "FINISHED":
                return EncounterStatus.FINISHED;
            case "BILLED":
                return EncounterStatus.BILLED;
            case "CANCELLED":
                return EncounterStatus.CANCELLED;
            default:
                return null;
        }
    }

    public static String getCheckBoxColor(EncounterStatus status){
        switch (status){
            case ARRIVED:
                return "#03A9F4";
            case IN_PROGRESS:
                return "#3ADC6A";
            case FINISHED:
                return "#00BCD4";
            case BILLED:
                return "#efff4765";
            case CANCELLED:
                return "#FFFFFF";
            default:
                return "#03A9F4";
        }
    }
}

