package com.example.curie;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;
import java.util.regex.*;

import com.example.curie.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class WelcomeScreen extends AppCompatActivity {

    private static final String TAG ="WelcomeScreen" ;
    private EditText userId;
    String role;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome_screen);
        userId = (EditText) findViewById( R.id.user_id );
    }
    public void startApplication(View view) {

         final String data = userId.getText().toString();
         final boolean valid = Pattern.matches("^[0-9]+$", data);
         if(valid){
             FirebaseDatabase database = FirebaseDatabase.getInstance();
             DatabaseReference myRef = database.getReference("curie").child("users");
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                       int check =0;
                        for(int i=0;i<dataSnapshot.getChildrenCount();i++){

                            String userid = dataSnapshot.child(i+"").child("id").getValue().toString();

                            if(data.equals(userid)){
try {
    User user = new User();
    user.setId(Integer.parseInt(dataSnapshot.child(i + "").child("designation").getValue().toString()));
    user.setDesignation(dataSnapshot.child(i + "").child("designation").getValue().toString());
    user.setHospital(dataSnapshot.child(i + "").child("hospital").getValue().toString());
    user.setName(dataSnapshot.child(i + "").child("name").getValue().toString());
    user.setRole(dataSnapshot.child(i + "").child("role").getValue().toString());
    PreferenceManager.saveCurrentUser(WelcomeScreen.this,user);

}catch (Exception e){

}
                                role = dataSnapshot.child(""+i).child("role").getValue().toString();
                                check = 1;
                                break;
                            }
                        }
                        if(check == 1){
                           if(role.equals("Nurse")){

                                startActivity( new Intent( WelcomeScreen.this, Nurse.class ));
                                finish();
                            }else if (role.equals("Doctor")){
                                startActivity( new Intent( WelcomeScreen.this, Doctor.class ));
                                finish();
                            }
                        }else{

                            Toast.makeText( getApplicationContext(),"Invalid User Id",Toast.LENGTH_SHORT ).show();
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError error) {
                        Log.w(TAG, "No Internet Connection", error.toException());
                    }
                });
         }else{
             Toast.makeText( getApplicationContext(),"Please enter valid User Id",Toast.LENGTH_SHORT ).show();
         }
    }
}
