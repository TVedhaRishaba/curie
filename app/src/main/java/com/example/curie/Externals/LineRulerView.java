package com.example.curie.Externals;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.example.curie.R;
import com.tistory.dwfox.dwrulerviewlibrary.utils.DWUtils;



/**
 * Created by Balasubramaniyam on 6/12/2017.
 */

public class LineRulerView extends View {

    private Paint paint,paint_selected;
    int selectedItem = 0;
    private Paint textPaint,textPaint_Selected,textPaint_Selected_small;
    private DashPathEffect dashPathEffect;
    private Path backGroundPath;

    private float bigUnitLineHeight = 0f;
    private float samllUnitLineHeight = 0f;

    private float MAX_DATA = 150;
    private float MIN_DATA = 80;

    private int viewHeight = 0;
    private int viewWidth = 0;

    private int showRangeValue = 5;

    private int valueMultiple = 1;
    public final String TAG = this.getClass().getCanonicalName();

    // 1  5개마다 ( Default)
    // 2  특정 배수 마다
    private int displayNumberType = 1;

    public static final int DISPLAY_NUMBER_TYPE_SPACIAL_COUNT = 1;
    public static final int DISPLAY_NUMBER_TYPE_MULTIPLE = 2;
    private int valueTypeMultiple = 5;

    private int longHeightRatio = 15;
    private int shortHeightRatio = 10;
    private int baseHeightRatio = 3;
    private int minStart;
    private int minEnd;
    private Paint paint_danger;


    public LineRulerView(Context context) {
        super(context);
        init(context);
    }

    public LineRulerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public LineRulerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LineRulerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
        Log.d( TAG, "setSelectedItem: item selected invalidating" );
        invalidate();
    }

    private void init(Context context) {
        paint = new Paint();
        paint.setColor(context.getResources().getColor( R.color.colorPrimary ) );
        paint.setStrokeWidth(3f);
        paint.isAntiAlias();
        paint.setStyle(Paint.Style.STROKE);

        paint_danger = new Paint();
        paint_danger.setColor(context.getResources().getColor( R.color.colorDanger ) );
        paint_danger.setStrokeWidth(3f);
        paint_danger.isAntiAlias();
        paint_danger.setStyle(Paint.Style.STROKE);

        paint_selected = new Paint();
        paint_selected.setColor(context.getResources().getColor( R.color.colorAccent ) );
        paint_selected.setStrokeWidth(3f);
        paint_selected.isAntiAlias();
        paint_selected.setStyle(Paint.Style.STROKE);

        textPaint = new Paint();
        textPaint.setColor(context.getResources().getColor( R.color.colorPrimary ));
        textPaint.isAntiAlias();
        textPaint.setTextSize(DWUtils.sp2px(context, 12));
        textPaint.setTextAlign(Paint.Align.CENTER);

        textPaint_Selected = new Paint();
        textPaint_Selected.setColor(context.getResources().getColor( R.color.colorAccent ));
        textPaint_Selected.isAntiAlias();
        textPaint_Selected.setTextSize(DWUtils.sp2px(context, 14));
        textPaint_Selected.setTextAlign(Paint.Align.CENTER);


        textPaint_Selected_small = new Paint();
        textPaint_Selected_small.setColor(context.getResources().getColor( R.color.colorAccent ));
        textPaint_Selected_small.isAntiAlias();
        textPaint_Selected_small.setTextSize(DWUtils.sp2px(context, 14));
        textPaint_Selected_small.setTextAlign(Paint.Align.CENTER);



        invalidate();
    }

    public LineRulerView setMaxValue(float maxValue) {
        this.MAX_DATA = maxValue;
        return this;
    }

    public LineRulerView setMinValue(float minValue) {
        this.MIN_DATA = minValue;
        return this;
    }

    public LineRulerView setMinMaxValue(float minValue, float maxValue) {
        this.MIN_DATA = minValue;
        this.MAX_DATA = maxValue;
        return this;
    }

    public LineRulerView setValueMultiple(int valueMultiple) {
        this.valueMultiple = valueMultiple;
        return this;
    }

    public void setMultipleTypeValue(int valueTypeMultiple) {
        this.displayNumberType = DISPLAY_NUMBER_TYPE_MULTIPLE;
        this.valueTypeMultiple = valueTypeMultiple;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        viewHeight = getMeasuredHeight();
        viewWidth = getMeasuredWidth();
        viewWidth = viewWidth;

        Log.d( "LINE RULER", "onDraw: view width "+viewWidth+" max - min "+MAX_DATA+" - "+MIN_DATA );
        float viewInterval = (float) viewWidth / (MAX_DATA - MIN_DATA);
        Log.d( "LINE RULER", "onDraw: item "+viewInterval );
        canvas.drawLine(0, 0, 0, viewHeight / longHeightRatio * baseHeightRatio, paint);
        for (int i = 1; i < (MAX_DATA - MIN_DATA); i++) {
            Log.d( TAG, "onDraw: item pos "+(viewInterval * i) );
            Log.d( TAG, "onDraw: selected item "+((i + MIN_DATA) * valueMultiple));
            if (((int) (i + MIN_DATA) * valueMultiple) == selectedItem ){
                if (displayNumberType == DISPLAY_NUMBER_TYPE_MULTIPLE) {

                    if (((int) (i + MIN_DATA) * valueMultiple) % valueTypeMultiple == 0) {
                       /* if ((i + MIN_DATA) <= minEnd && (i + MIN_DATA ) >= minStart  ){

                        }else{

                        }*/
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio, paint_selected);
                        canvas.drawText("" + ((int) (i + MIN_DATA) * valueMultiple), viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio + DWUtils.sp2px(getContext(), 14), textPaint_Selected);
                    } else {
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / longHeightRatio * baseHeightRatio, paint_selected);
                        canvas.drawText("" + ((int) (i + MIN_DATA) * valueMultiple), viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio + DWUtils.sp2px(getContext(), 14), textPaint_Selected_small);
                    }


                } else {
                    if (i % 5 == 0) {
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio, paint_selected);
                        canvas.drawText("" + ((int) (i + MIN_DATA) * valueMultiple), viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio + DWUtils.sp2px(getContext(), 14), textPaint_Selected);
                    } else {
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / longHeightRatio * baseHeightRatio, paint_selected);
                        canvas.drawText("" + ((int) (i + MIN_DATA) * valueMultiple), viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio + DWUtils.sp2px(getContext(), 14), textPaint_Selected_small);
                    }

                }
            }else{
                if (displayNumberType == DISPLAY_NUMBER_TYPE_MULTIPLE) {

                    if (((int) (i + MIN_DATA) * valueMultiple) % valueTypeMultiple == 0) {
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio, paint);
                        canvas.drawText("" + ((int) (i + MIN_DATA) * valueMultiple), viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio + DWUtils.sp2px(getContext(), 14), textPaint);
                    } else {
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / longHeightRatio * baseHeightRatio, paint);
                    }

                } else {
                    if (i % 5 == 0) {
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio, paint);
                        canvas.drawText("" + ((int) (i + MIN_DATA) * valueMultiple), viewInterval * i, viewHeight / shortHeightRatio * baseHeightRatio + DWUtils.sp2px(getContext(), 14), textPaint);
                    } else {
                        canvas.drawLine(viewInterval * i, 0, viewInterval * i, viewHeight / longHeightRatio * baseHeightRatio, paint);
                    }

                }
            }


        }

        canvas.drawLine(viewWidth, 0, viewWidth, viewHeight / longHeightRatio * baseHeightRatio, paint);

        super.onDraw(canvas);
    }

    public void setMediumValue(int minStart, int minEnd) {
        this.minStart = minStart;
        this.minEnd = minEnd;
        invalidate();
    }
}
