package com.example.curie.Externals;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.curie.R;
import com.tistory.dwfox.dwrulerviewlibrary.utils.DWUtils;
import com.tistory.dwfox.dwrulerviewlibrary.view.ObservableHorizontalScrollView;



/**
 * Created by Balasubramaniyam on 6/12/2017.
 */

public class ScrollingValuePicker extends FrameLayout {

    private static final String TAG = "SVP";
    private View mLeftSpacer;
    private View mRightSpacer;

    public void setListener(onScrollValueChangeListener listener) {
        this.listener = listener;
    }

    public onScrollValueChangeListener listener;
    private LineRulerView lineRulerView;
    private ObservableHorizontalScrollView mScrollView;
    private float viewMultipleSize = 3f;

    private int minStart,minEnd;
    private float maxValue = 200;
    private float minValue = 80;

    public float getMaxValue() {
        return maxValue;
    }

    public float getMinValue() {
        return minValue;
    }

    private float initValue = 0f;

    private int valueMultiple = 1;

    private int valueTypeMultiple = 5;

    public ScrollingValuePicker(Context context) {
        super(context);
        init(context);
    }

    public ScrollingValuePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScrollingValuePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ScrollingValuePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    public void setOnScrollChangedListener(final ObservableHorizontalScrollView.OnScrollChangedListener onScrollChangedListener) {
       // mScrollView.setOnScrollChangedListener(onScrollChangedListener);
    }

    public void setMaxValue(float minValue, float maxValue) {
        setMaxValue(minValue, maxValue, 1);
    }

    public void setMaxValue(float minValue, float maxValue, int valueMultiple) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.valueMultiple = valueMultiple;
        lineRulerView.setMaxValue(this.maxValue);
        lineRulerView.setMinValue(this.minValue);
        lineRulerView.setValueMultiple(this.valueMultiple);
    }

    public void setValueTypeMultiple(int valueTypeMultiple) {
        this.valueMultiple = valueTypeMultiple;
        lineRulerView.setMultipleTypeValue(valueTypeMultiple);
    }

    public void setInitValue(float initValue) {
        this.initValue = initValue;
        lineRulerView.setSelectedItem( (int) initValue );
        //DWUtils.scrollToValue( getScrollView(), );
    }

    public float getViewMultipleSize() {
        return this.viewMultipleSize;
    }

    public void setViewMultipleSize(float size) {
        this.viewMultipleSize = size;
    }

    public void setMediumValue(int minStart, int minEnd) {
        this.minStart = minStart;
        this.minEnd = minEnd;
        this.lineRulerView.setMediumValue(minStart,minEnd);
    }

    public interface onScrollValueChangeListener{
        public void onValueChanged(int newPosition);
    }

    private void init(Context context) {
        mScrollView = new ObservableHorizontalScrollView(context);
        mScrollView.setHorizontalScrollBarEnabled(false);
        mScrollView.setOnScrollChangedListener( new ObservableHorizontalScrollView.OnScrollChangedListener() {
            @Override
            public void onScrollChanged(ObservableHorizontalScrollView observableHorizontalScrollView, int i, int i1) {

            }

            @Override
            public void onScrollStopped(int i, int i1) {
                int cp = DWUtils.getValueAndScrollItemToCenter(mScrollView
                        , i
                        , i1
                        , getMaxValue()
                        , getMinValue()
                        , getViewMultipleSize());

                Log.d("TAGGG", "Value STOP at inside : " +cp
                        );

                lineRulerView.setSelectedItem( cp );
                if (listener != null){
                    listener.onValueChanged( cp );
                }

            }


        } );
        addView(mScrollView);

        final LinearLayout container = new LinearLayout(context);
        mScrollView.addView(container);

        mLeftSpacer = new View(context);
        mRightSpacer = new View(context);

        lineRulerView = new LineRulerView(context);
        container.addView(lineRulerView);
        container.addView(mLeftSpacer, 0);
        container.addView(mRightSpacer);


        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getWidth() != 0) {
                    DWUtils.scrollToValue(getScrollView(), initValue, maxValue, minValue, viewMultipleSize);
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });

    }

    public ObservableHorizontalScrollView getScrollView() {
        return mScrollView;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Paint paint = new Paint();
        paint.setColor(getResources().getColor( R.color.colorAccent ));
        paint.setStrokeWidth(3f);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        Path path = new Path();
        path.moveTo(getWidth() / 2 - 8, 0);
        path.lineTo(getWidth() / 2, 16);
        path.lineTo(getWidth() / 2 + 8, 0);
        canvas.drawPath(path, paint);
        super.onDraw(canvas);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (changed) {

            final int width = getWidth();

            final ViewGroup.LayoutParams leftParams = mLeftSpacer.getLayoutParams();
            leftParams.width = width / 2;
            mLeftSpacer.setLayoutParams(leftParams);

            final ViewGroup.LayoutParams rulerViewParams = lineRulerView.getLayoutParams();
            rulerViewParams.width = (int) (width * viewMultipleSize);  // set RulerView Width
            lineRulerView.setLayoutParams(rulerViewParams);
            lineRulerView.invalidate();


            final ViewGroup.LayoutParams rightParams = mRightSpacer.getLayoutParams();
            rightParams.width = width / 2;
            mRightSpacer.setLayoutParams(rightParams);

            invalidate();

        }
    }
}
